﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App
{
    class Program
    {
        static void Main(string[] args)
        {
            List<List<int>> lista = new List<List<int>>();
            lista[0].Add(11);
            lista[0].Add(2);
            lista[0].Add(4);

            lista[1].Add(4);
            lista[1].Add(5);
            lista[1].Add(6);

            lista[0].Add(10);
            lista[0].Add(8);
            lista[0].Add(-12);

            // Tambien se puede resolver con un array cambiando lista.Count()
            // por arr.GetLength(index)
            // int[,] arr = { { 11, 2, 4 }, { 4, 5, 6 }, { 10, 8, -12 } };
            int columna = 0;
            int sumRtL = 0;
            int sumLtR = 0;
            int result = 0;
            for(int i = 0; i < lista.Count(); i++)
            {
                sumRtL += lista[i][columna];
                sumLtR += lista[i][(lista.Count() - (columna+1))];
                columna++;
            }

            result = sumRtL - sumLtR;

            Console.WriteLine(Math.Abs(result).ToString());
            Console.ReadKey();
        }
    }
}
